#include <stdio.h>
#include <string.h>
#include<ctype.h>
#ifdef __WINDOWS__
#include <windows.h>
#else
#include   <stdlib.h>
#include   <unistd.h>
#include <limits.h>
#endif
#include "..\LogLib\log.h"
#include "config.h"

struct ST_File *g_File = NULL;
struct ST_File *g_EndNode = NULL;
struct ST_Server *g_Server = NULL;
struct ST_Board *g_Board = NULL;
struct ST_CMD *g_cmd = NULL;

int g_argc;
char **g_argv;
char g_szProjectPath[1024] = {'\0'};


/// @brief   把文件名和路径分离出去
///
/// @param   filePath   文件的全路径
/// @param   path   文件的路径
/// @param   name   文件名
/// @param   split   路径的分隔符Windows\ Linux /
///
/// @return
bool splitFilePath(const char *filePath, char *path, char *name, char split)
{
    char *p;
    if ((NULL == (p = (char *)strrchr(filePath, split))) || (strlen (p) > Max_FileNameLen) || (p - filePath > Max_PathLen))
    {
        LogoDebug("errorr format: argv[1] = [%s]\n", filePath);
        printf ("%s isn't a file\n", filePath);
        return false;
    }

    p++;
    //取得文件名
    strncpy (name, p, Max_FileNameLen);
    name[Max_FileNameLen] = '\0';
    //取得文件路径
    strncpy (path, filePath, p - filePath);
    path[p-filePath] = '\0';
//    LogoDebug ("file name = [%s]\n", name);
//    LogoDebug ("file path = [%s]\n", path);
    return true;
}


/// @brief   转换szFilename的斜杠
///
/// @param   szFilename   需要转换的文件名或者路径
/// @param   old   旧的斜杠
/// @param   new   新的斜杠
///
/// @return   返回szFilename的指针
const char *transformSlash (const char* szFilename, int oldSlash, int newSlash)
{
    char *p = strchr (szFilename, oldSlash);
    while (NULL != p)
    {
        *p = newSlash;
        p = strchr (szFilename, oldSlash);
    }
    return szFilename;
}

/// @brief   通过传入的本地文件名szFilename，获取远程文件名
///
/// @param   szFilename   本地文件名
/// @param   preRemotePath   远程目录的前缀
/// @param   preLocalPath   本地目录的前缀
///
/// @return   远程文件的文件名
char *getRemoteFilename (const char* szFilename, const char* preRemotePath, const char* preLocalPath)
{
    char s_szFilename[1024 + 1];
    char* p = strstr (szFilename, preLocalPath);

    if (NULL != p && p == szFilename)
    {
        strcpy (s_szFilename, preRemotePath);
        strncat (s_szFilename, szFilename + strlen (preLocalPath), sizeof (s_szFilename) - strlen (preRemotePath));
        transformSlash (s_szFilename, '\\', '/');
        return s_szFilename;
    }
    else
    {
        return NULL;
    }
}

/// @brief   去除多余的斜杠
///
/// @param   szFilePath   文件名或路径
/// @param   Slash   文件名或路径的斜杠
///
/// @return   返回新的文件名或者路径
char* processFilePath (char* szFilePath, char Slash)
{
    char szFine[3] = {Slash, Slash, '\0'};
    char *p = strstr (szFilePath, szFine);

    while (NULL != p)
    {
        *(++p) = '\0';
        strcat (szFilePath, p);
        p = strstr (szFilePath, szFine);
    }
}


void getValue (const char *buf, char *value, int iSize)
{
    char *pFirstMark;
    char *pEndMark;
    pFirstMark = (char *)strchr(buf, '\'');
    pEndMark = (char *)strrchr(buf, '\'');
    if (pFirstMark == pEndMark || (pEndMark - pFirstMark > iSize))
    {
        //只有一个“’”
        LogoDebug("error format: input is:  \n", buf);
        return;
    }
    pFirstMark++;
    while (pFirstMark != pEndMark)
    {
        *(value++) = *(pFirstMark++);
    }

    *value = '\0';
}


void addFilePath(const char *strPath, EN_PathType enPathType)
{
    //LogoDebug ("line = %d\n", __LINE__);
    //LogoDebug ("FilePath = [%s]\n", strPath);
    if ('\0' != g_EndNode->strPath[enPathType][0])
    {
        g_EndNode->next = new struct ST_File;
        g_EndNode = g_EndNode->next;
    }

    strcat(g_EndNode->strPath[enPathType], strPath);
}


void addFile (char *strFileName)
{
   if (NULL != g_EndNode)
   {
        processFilePath (strFileName, '\\');
        strcat (g_EndNode->szFileList, strFileName);
        strcat (g_EndNode->szFileList, "|");
   }
}

#ifndef __WINDOWS__
void get_exe_name(char* filename)
{
    enterLine;
//    int rslt = readlink("/proc/self/exe", filename, PATH_MAX);
    static char buf[22222];
    char ptr[256];
    sprintf(ptr,"/proc/%d/exe",getpid());

    int rslt = readlink(ptr, buf, sizeof (buf));

    printf ("\nptr = [%s]\n", ptr);
    printf ("buf = [%s]\n", buf);
    getchar ();
    enterLine;
    if (rslt < 0 || rslt >= PATH_MAX)
    {
        *filename = '\0';
    }
    else
    {
        filename[rslt] = '\0';
    }
}
#endif

void setProjectPath (char *szProject)
{
    char *p = strrchr(szProject, '\\');
//    *(p + 1) = NULL;
    strncpy (g_szProjectPath, szProject, p - szProject + 1);
//    printf ("g_szProjectPath = [%s]\n", g_szProjectPath);
}

char* getConfigFile (char *szProject)
{
    static char szFilename[1024] = {'\0'};
    if ('\0' == szFilename[0])
    {
        char *p = strrchr(szProject, '\\');
//        printf ("Project = [%s]\n", p);
        strcpy (szFilename, g_szProjectPath);
        strcat (szFilename, CONFIG_PATH);
        strcat (szFilename, p + 1);
        strcat (szFilename, ".cfg");
//        printf ("config = [%s]\n", szFilename);
//        LogoDebug ("szFilename = [%s]", szFilename);
    }
    return szFilename;
}

char* getSaveFile (char *szProject)
{
    static char szFilename[1024] = {'\0'};
    if ('\0' == szFilename[0])
    {
        char *p = strrchr(szProject, '\\');
//        printf ("Project = [%s]\n", p);
        strcpy (szFilename, g_szProjectPath);
        strcat (szFilename, SAVE_PATH);
        strcat (szFilename, p + 1);
        strcat (szFilename, ".txt");
//        printf ("SaveFile = [%s]\n", szFilename);
//        LogoDebug ("szFilename = [%s]", szFilename);
    }
    return szFilename;
}


//返回配置文件所在的绝对路径
const char  *getFilePath(const char *pFilename, const char * strPath)
{
    static char szFilePath[1024];

    strcpy (szFilePath, g_szProjectPath);
    strcat (szFilePath, strPath);
    strcat (szFilePath, pFilename);
    return szFilePath;
}


char *filtrationSpace(char *p)
{
    if ('\0' == *p || NULL == p)
    {
        return p;
    }

    //去除左边
    while (isspace (*p)  || '\r' == *p ||  '\n' == *p)
    {
        p++;
    }
    //p--;

    //去除右边
    int n = strlen (p) - 1;
    while (isspace (p[n]) || '\r' == p[n] ||  '\n' == p[n])
    {
        n--;
    }

    p[++n] = '\0';
//    LogoDebug ("p = [%s]", p);
    return p;
}


bool modifyCfg(const char * cfgFile, const char *pItem, const char *pNew)
{
    FILE *oldFp;
    FILE *newFp;
    const char *tempFile = "temp.cfg";
    bool bFinsh = false;
    char buf[1024];
    LogoDebug("open config file = [%s]", cfgFile);

    if ((NULL != (oldFp= fopen(cfgFile, "r"))) &&
        (NULL != (newFp= fopen(tempFile, "w+"))))
    {
        while (1)
        {
            if (NULL !=fgets(buf, sizeof(buf), oldFp))
            {
                if (!bFinsh)
                {
                    char *p;
                    if (NULL != (p = strstr (buf, pItem)) && '=' == *filtrationSpace(p + strlen (pItem)))
                    {
                        //修改buf的值
                        strcpy (buf, pItem);
                        strcat (buf, " = '");
                        strcat (buf, pNew);
                        strcat (buf, "'\r\n");
                    }
                }
                fputs (buf, newFp);
            }
            else
            {
                if (0 == feof (oldFp))
                {
                    LogoDebug ("read [%s] false", cfgFile);
                    fclose (oldFp);
                    fclose (newFp);
                    return false;
                }
                break;
            }
        }
    }
    else
    {
        LogoDebug ("open [%s] false", cfgFile);
        return false;
    }

    fclose (oldFp);
    fclose (newFp);


    char bakFile[256];
    strcpy (bakFile, cfgFile);
    strcat (bakFile, ".bak");

    //先删除原来的备份文件
    if (0 !=remove(bakFile))
    {
        system("chdir");
        perror ("remove");
    }
#ifdef __WINDOWS__
    //先备份上一次的配置文件
    if (0 == MoveFile( (LPCTSTR)cfgFile, (LPCTSTR)bakFile))
    {
        system("chdir");
        LogoDebug("File: temp.ttl cannot be removed, cause = %s \n", GetLastError() );
    }
    //重命名
    if (0 == MoveFile( (LPCTSTR) tempFile, (LPCTSTR)cfgFile))
    {
        system("chdir");
        LogoDebug("File: temp.ttl cannot be removed, cause = %s \n", GetLastError() );
    }
#else
    if (rename(cfgFile, bakFile) < 0)
    {
        perror ("rename");
    }
    if (rename(tempFile, cfgFile) < 0)
    {
        perror ("rename");
    }
#endif
    return bFinsh;
}

bool readCfg(const char * cfgFile, bool bReadFileInfo)
{
    FILE *fp;
    char buf[1024];

    LogoDebug("open config file = [%s]", cfgFile);
    if (NULL != (fp = fopen(cfgFile, "r")))
    {
        g_cmd = new struct ST_CMD;
        g_Server = new ST_Server;
        g_Board = new  struct ST_Board;
        if (bReadFileInfo)
        {
            g_File = new struct ST_File;
            g_EndNode = g_File;
        }

        //打开成功
        while(1)
        {
            if (NULL !=fgets(buf, sizeof(buf), fp))
            {
                //读取成功
//                LogoDebug ("buf = %s", buf);
//                fprintf(stderr, "buf = %s", buf);
                if ('#' == buf[0] || '\0' == buf[0])
                {
                    //第一个字符是#时，为注释
                    continue;
                }
                ///////////////////板和服务器配置信息///////////////////
                else if (strstr(buf, "ServerIP") && strstr(buf, "="))
                {
                    //为服务器IP
                    getValue(buf, g_Server->strIP, Max_IPLen);
                }
                else if (strstr(buf, "ServerUsername") && strstr(buf, "="))
                {
                    //为服务器用户名
                    getValue(buf, g_Server->strUserName, Max_IPLen);
                }
                else if (strstr(buf, "ServerPassword") && strstr(buf, "="))
                {
                    //为服务器密码
                    getValue(buf, g_Server->strPassword, Max_IPLen);
                }
                else if (strstr(buf, "ServerFileTransferProtocol") && strstr(buf, "="))
                {
                    //服务器文件传输协议支持：SMB、FTP、SFTP
                    getValue(buf, g_Server->strFileTransferProtocol, sizeof (g_Server->strFileTransferProtocol));
                }
                else if (strstr(buf, "ServerExecuteCMDProtocol") && strstr(buf, "="))
                {
                    //服务器执行命令协议支持：TELNETD、SSH
                    getValue(buf, g_Server->strExecuteCMDProtocol, sizeof (g_Server->strExecuteCMDProtocol));
                }
                else if (strstr(buf, "BoardIP") && strstr(buf, "="))
                {
                    //为板子IP
                    getValue(buf, g_Board->strIP, Max_IPLen);
                }
                else if (strstr(buf, "BoardUsername") && strstr(buf, "="))
                {
                    //为板子用户名
                    getValue(buf, g_Board->strUserName, Max_IPLen);
                }
                else if (strstr(buf, "BoardPassword") && strstr(buf, "="))
                {
                    //为板子密码
                    getValue(buf, g_Board->strPassword, Max_IPLen);
                }
                else if (strstr(buf, "BoardExecuteCMDProtocol") && strstr(buf, "="))
                {
                    //为板子密码
                    getValue(buf, g_Board->strExecuteCMDProtocol, Max_IPLen);
                }

                /////////////////////命令/////////////////////////
                if(strstr(buf, "BuildCMD") && strstr(buf, "="))
                {
                    //拷贝GUI
                    getValue(buf, g_cmd->szBuildCMD, sizeof (g_cmd->szBuildCMD));
                }
                else if (strstr(buf, "Copy2BoardCMD") && strstr(buf, "="))
                {
                     //拷贝后台
                    getValue(buf, g_cmd->szCopy2Board, sizeof (g_cmd->szCopy2Board));
                }

                ///////////////////////telnet提示符//////////////////
                else if (strstr(buf, "ServerLoginPrompt") && strstr(buf, "="))
                {
                    //提示输入用户名
                    getValue(buf, g_Server->strLoginPrompt, Max_IPLen);
                }
                if(strstr(buf, "ServerPasswdPrompt") && strstr(buf, "="))
                {
                    //提示输入密码
                    getValue(buf, g_Server->strPasswdPrompt, 1024);
                }
                else if (strstr(buf, "ServerLastLoginPrompt") && strstr(buf, "="))
                {
                     //
                    getValue(buf, g_Server->strLastLoginPrompt, 1024);
                }
                else if (strstr(buf, "ServerLoginIncorrectPrompt") && strstr(buf, "="))
                {
                    //
                    getValue(buf, g_Server->strLoginIncorrectPrompt, 1024);
                }
                else if (strstr(buf, "ServerFinishPrompt") && strstr(buf, "="))
                {
                     //执行完一条命令的提示符
                    getValue(buf, g_Server->strFinishPrompt, 1024);
                }
                else if (strstr(buf, "BoardLoginPrompt") && strstr(buf, "="))
                {
                     //
                    getValue(buf, g_Board->strLoginPrompt, 1024);
                }
                else if (strstr(buf, "BoardPasswdPrompt") && strstr(buf, "="))
                {
                     //
                    getValue(buf, g_Board->strPasswdPrompt, 1024);
                }
                else if (strstr(buf, "BoardLastLoginPrompt") && strstr(buf, "="))
                {
                     //
                    getValue(buf, g_Board->strLastLoginPrompt, 1024);
                }
                else if (strstr(buf, "BoardLoginIncorrectPrompt") && strstr(buf, "="))
                {
                     //
                    getValue(buf, g_Board->strLoginIncorrectPrompt, 1024);
                }
                else if (strstr(buf, "BoardFinishPrompt") && strstr(buf, "="))
                {
                     //
                    getValue(buf, g_Board->strFinishPrompt, 1024);
                }
                //////////////////////////////文件信息//////////////////////////////////
                if(bReadFileInfo && strstr(buf, "LocalPath") && strstr(buf, "="))
                {
                    //为下面文件列表的上传路径
                    char strFilePath[1024 + 1];
                    getValue(buf, strFilePath, 1024);
                    strcat (strFilePath, "\\");
                    processFilePath (strFilePath, '\\');
                    addFilePath(strFilePath, EN_LocalPath);
                }
               else if (bReadFileInfo && strstr(buf, "ServerPath") && strstr(buf, "="))
               {
                    //为下面文件列表的上传路径
                    char strFilePath[1024 + 1];
                    getValue(buf, strFilePath, 1024);
                    strcat (strFilePath, "/");
                    processFilePath (strFilePath, '/');
                    addFilePath(strFilePath, EN_ServerPath);
                }
                else if (bReadFileInfo)
                {
                    //读到文件列表
                    addFile(filtrationSpace(buf));
                }
            }
            else
            {
                //读取失败
                int iErr = feof (fp);
                fclose (fp);

                 if (0 == iErr)
                {
                    LogoDebug ("read [%s] false", cfgFile);
                    return false;
                }
                LogoDebug ("read config succeed!");
                return true;

            }
        }
    }
    else
    {
        //打开失败
        LogoDebug("open config file %s false!!! \n", cfgFile);
        printf ("open config file %s false!!! \n", cfgFile);
        fflush(stdout);
        //getchar();
        return false;
    }
}
