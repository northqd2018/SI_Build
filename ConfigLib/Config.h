#pragma once

//#ifdef CONFIGLIB_EXPORTS
//    #undef CONFIGLIB_EXPORTS
//    #define CONFIGLIB_EXPORTS  __declspec(dllexport)
//#else
//    #define CONFIGLIB_EXPORTS  __declspec(dllimport)
//#endif
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#define CONFIGLIB_EXPORTS

#define Max_PathLen 512
#define Max_FileNameLen 256
#define Max_IPLen 16

#define CONFIG_PATH "config\\"
#define SAVE_PATH "save\\"
#define LOG_PATH "log\\"

struct ST_ArgvOpt
{
    bool bUploadAll;
    bool bCopyToBoard;
    char szSaveFile[256];
    char szUploadFile[256];
    char szProjectName[256];

    ST_ArgvOpt()
    {
        bUploadAll = false;
        bCopyToBoard = false;
        szSaveFile[0] = '\0';
        szUploadFile[0] = '\0';
        szProjectName[0] = '\0';
    }
};

struct ST_CMD
{
    char szBuildCMD[1025];
    bool bNeedBuild;
    char szCopy2Board[1025];
    bool bNeedCopy2Board;
    ST_CMD ()
    {
        szBuildCMD[0] = '\0';
        szCopy2Board[0] = '\0';

        bNeedBuild = false;
        bNeedCopy2Board = false;
    }
};


enum EN_PathType
{
    EN_LocalPath = 0,
    EN_ServerPath,
    EN_MaxPath
};

struct ST_File
{
    char strPath[EN_MaxPath][1024 + 1];
    char * szFileList;
    char szCopyCmd[1024];
    struct ST_File *next;

    ST_File()
    {
        strPath[EN_LocalPath][0] = '\0';
        strPath[EN_ServerPath][0] = '\0';
        szCopyCmd[0] = '\0';
        next = '\0';
        szFileList = new char[2 * 1024 * 1024];
        *szFileList = '\0';
    }
};


struct ST_Server
{
    char strUserName[16 + 1];
    char strPassword[16 + 1];
    char strFileTransferProtocol[16 + 1];
    char strExecuteCMDProtocol[16 + 1];
    char strIP[16 + 1];
    char strLoginPrompt[64];
    char strPasswdPrompt[64];
    char strLastLoginPrompt[64];
    char strLoginIncorrectPrompt[64];
    char strFinishPrompt[64];

    ST_Server ()
    {
        strUserName[0] = '\0';
        strPassword[0] = '\0';
        strIP[0] = '\0';
        strLoginPrompt[0] = '\0';
        strPasswdPrompt[0] = '\0';
        strLastLoginPrompt[0] = '\0';
        strLoginIncorrectPrompt[0] = '\0';
        strFinishPrompt[0] = '\0';
        strFileTransferProtocol[0] = '\0';
        strExecuteCMDProtocol[0] = '\0';
    }
};


struct ST_Board
{
    char strUserName[16 + 1];
    char strPassword[16 + 1];
    char strIP[16 + 1];
    char strLoginPrompt[64];
    char strPasswdPrompt[64];
    char strLastLoginPrompt[64];
    char strLoginIncorrectPrompt[64];
    char strFinishPrompt[64];
    char strExecuteCMDProtocol[16 + 1];
    ST_Board ()
    {
        strUserName[0] = '\0';
        strPassword[0] = '\0';
        strIP[0] = '\0';
        strLoginPrompt[0] = '\0';
        strPasswdPrompt[0] = '\0';
        strLastLoginPrompt[0] = '\0';
        strLoginIncorrectPrompt[0] = '\0';
        strFinishPrompt[0] = '\0';
        strExecuteCMDProtocol[0] = '\0';
    }
};

extern struct ST_File *g_File;
extern struct ST_File *g_EndNode;
extern struct ST_Server *g_Server;
extern struct ST_Board *g_Board;
extern struct ST_CMD *g_cmd;
void setProjectPath (char *szProject);
char* getConfigFile (char *szProject);
char* getSaveFile (char *szProject);
char *getRemoteFilename (const char* szFilename, const char* preRemotePath, const char* preLocalPath);
const char *transformSlash (const char* szFilename, int oldSlash, int newSlash);
//返回配置文件所在的绝对路径
CONFIGLIB_EXPORTS const char  *getFilePath(const char *pFilename, const char * strPath);
CONFIGLIB_EXPORTS bool splitFilePath(const char *filePath, char *path, char *name, char split);
CONFIGLIB_EXPORTS bool readCfg(const char * cfgFile, bool bReadFileInfo);
CONFIGLIB_EXPORTS bool modifyCfg(const char * cfgFile, const char *pItem, const char *pNew);
//过滤空格
CONFIGLIB_EXPORTS char * filtrationSpace(char *p);
void addFile (char *strFileName);
void addFilePath(const char *strPath, EN_PathType enPathType);
CONFIGLIB_EXPORTS void getValue (const char *buf, char *value, int iSize);
