#include <stdarg.h>
#include "log.h"
#include "../ConfigLib/Config.h"


FILE *g_FpLog = NULL;

int initLog (const char* filename)
{
    if(NULL == (g_FpLog = fopen(getFilePath(filename, LOG_PATH), "w+")))
    {
        printf("fopen(%s, \"w+\") fails\n", getFilePath(filename, LOG_PATH));
        fflush (stdout);
        getchar ();
        return -1;
    }
    return 0;
}

void LOGDEBUG(const char *fmt, ...)
{
    if (NULL != g_FpLog)
    {
        va_list vaList;
        va_start(vaList, fmt);
        vfprintf(g_FpLog, fmt, vaList);
        va_end(vaList);
        fflush(g_FpLog);
    }
}

