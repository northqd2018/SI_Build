#ifndef _LOG_H_
#define _LOG_H_
#include <stdio.h>
#define LOGLIB_EXPORTS
//#ifdef LOGLIB_EXPORTS
//    #undef LOGLIB_EXPORTS
//    #define LOGLIB_EXPORTS __declspec(dllexport)
//#else
//    #define LOGLIB_EXPORTS __declspec(dllimport)
//#endif
int initLog (const char* filename);

extern LOGLIB_EXPORTS FILE *g_FpLog;
LOGLIB_EXPORTS void LOGDEBUG(const char *fmt, ...);

#define LogoDebug LOGDEBUG("\r\n[%s:%s:%d]", __FILE__, __FUNCTION__, __LINE__); LOGDEBUG
#define MY_log printf("\r\n[%s:%s:%d]", __FILE__, __FUNCTION__, __LINE__); printf
//#define enterLine  LogoDebug("Enter");
//#define exitLine  LogoDebug("Exit");
#define enterLine  fprintf(stderr, "\r\nEnter[%s:%s:%d]", __FILE__, __FUNCTION__, __LINE__);
#define exitLine  fprintf(stderr, "\r\nExit[%s:%s:%d]", __FILE__, __FUNCTION__, __LINE__);
#endif //_LOG_H_
