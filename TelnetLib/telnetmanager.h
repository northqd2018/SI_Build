#pragma once
#include <string>
//#include <winsock2.h>
#include "ProtolProcess.h"


class CTelnetManager
{
    enum
    {
        enSocketNotConnect = 0,
        enSocketConnected
    };

public:
    CTelnetManager();
    ~CTelnetManager();

//    bool Init(HWND telnetOutput = NULL);
    bool Connect(const std::string& strUserName, const std::string& strUserPasswd,
             const std::string& strIP, int iPort = 23);
    bool ExcuteShellCmd(const char *strShellCmd, std::string& strRes);
    void setPromptStr (char *strLogin, char *strPasswd, char *strLastLogin, char *strLoginIncorrect);
    bool DisConnect();
private:
    bool IsLoginSuccess();

    //BOOL GetResult(const std::string& strCmd, const std::string& strSrc, std::string& strRes);
    bool GetResult(const char *strCmd);
private:
    std::string m_strLogin;
    std::string m_strPasswd;
    std::string m_strLastLogin;
    std::string m_strLoginIncorrect;
    std::string m_strFinish;

    int m_iSocketState;
    std::string m_strSrvIP;
    int m_iPort;
    std::string m_strUserName;
    std::string m_strUserPasswd;

    int m_hSocket;
    char m_szEnterKey[3];
    bool IsCanExcuteShellCmd;
//    HWND m_telnetOutput;
    //BOOL IsExcuteGetCmdReturnCode;
    //BOOL IsEchoCmdReturnCode;
    //BOOL m_bFindCmd;

    CProtolProcess *m_ProtolProcess;
};
