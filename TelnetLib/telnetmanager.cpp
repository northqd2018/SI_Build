#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdarg.h>
#include "telnetmanager.h"
#include "..\LogLib\log.h"
//命令执行完成提示
#define FINISH_PROMPT "MyFinish"
std::string g_strRecvData;

CTelnetManager::CTelnetManager()
{
    m_hSocket = -1;
    m_iSocketState = enSocketNotConnect;
    m_ProtolProcess = new CProtolProcess;
    IsCanExcuteShellCmd = false;

    m_szEnterKey[0] = '\r';
    m_szEnterKey[1] = '\n';
    m_szEnterKey[2] = '\0';

//    m_telnetOutput = NULL;

    m_strFinish = FINISH_PROMPT;
    m_strLogin = "login: ";
    m_strPasswd = "Password: ";
    m_strLastLogin = "Last login: ";
    m_strLoginIncorrect = "Login incorrect";

    //IsExcuteGetCmdReturnCode = false;
    //IsEchoCmdReturnCode = false;
}

CTelnetManager::~CTelnetManager()
{
    if (m_hSocket > 0)
    {
        close(m_hSocket);
        m_hSocket = -1;
    }
}


bool CTelnetManager::Connect(const std::string& strUserName, const std::string& strUserPasswd,
                         const std::string& strIP, int iPort)
{
    if (m_iSocketState == enSocketConnected)
    {
        //如果已经有连接，则把上一次的连接断开
        LogoDebug("DisConnect the first connect.");
        DisConnect();
    }

    //m_iSocketState = enSocketNotConnect;
    m_hSocket = socket(AF_INET, SOCK_STREAM, 0);
    if ( m_hSocket < 0)
    {
        perror ("socket");
        LogoDebug ("false in socket()[%s]", strerror (errno));
        return false;
    }

    m_strSrvIP = strIP;
    m_iPort = iPort;
    m_strUserName = strUserName;
    m_strUserPasswd = strUserPasswd;

    int nRet;
    struct sockaddr_in tsockaddr_in;
    memset(&tsockaddr_in, 0x00, sizeof(sockaddr_in));
    tsockaddr_in.sin_family = AF_INET;
    tsockaddr_in.sin_port = htons(m_iPort);
    tsockaddr_in.sin_addr.s_addr = inet_addr(m_strSrvIP.c_str());
    if(tsockaddr_in.sin_addr.s_addr < 0)
    {
        struct hostent* phost;
        phost = (struct hostent*)gethostbyname(m_strSrvIP.c_str());
        if(phost==NULL){
            LogoDebug("Init socket s_addr error!");
            return false;
        }
        tsockaddr_in.sin_addr.s_addr =((struct in_addr*)phost->h_addr)->s_addr;
    }

    nRet = connect(m_hSocket,(sockaddr*)&tsockaddr_in,sizeof(sockaddr));
    if (nRet < 0)
    {
        perror ("connect host");
        LogoDebug("connect [%s]failed. errno[%s]", m_strSrvIP.c_str(), strerror (errno));
        return false;
    }

    m_iSocketState = enSocketConnected;

    if (true != IsLoginSuccess())
    {
        LogoDebug("Login failed.");
        fprintf(stderr, "Login[%s@%s] failed.", m_strUserName.c_str(), m_strSrvIP.c_str());
        return false;
    }

    IsCanExcuteShellCmd = true;
    LogoDebug("Login success\n");

    return true;
}

//bool CTelnetManager::Init(HWND telnetOutput)
//{
//    m_telnetOutput = telnetOutput;
//    //初始化网络
//    WSADATA wsaData;
//    int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
//    if (iResult != NO_ERROR)
//    {
//        LogoDebug("Error at WSAStartup().");

//        return false;
//    }

//    return true;
//}


bool CTelnetManager::IsLoginSuccess()
{
    enterLine
    char szDataBuf[1024];
    int nRet;
    char* scan;
    fd_set myFDSet;
    struct timeval tmVal;
    tmVal.tv_sec = 0;
    tmVal.tv_usec = 200 * 1000;

    int iLoopTimes = 0;
    int iMaxTimes = 15;

    std::string strCmd;
    bool bHaveSendUserName = false;
    bool bHaveSendPasswd = false;

    while (iLoopTimes < iMaxTimes)
    {
        FD_ZERO(&myFDSet);
        FD_SET(m_hSocket, &myFDSet);
        if (select(m_hSocket + 1, &myFDSet, NULL, NULL, &tmVal) < 0)
        {
            LogoDebug("select failed.....\n");

            continue;
        }

        if (!FD_ISSET(m_hSocket, &myFDSet))
        {
            iLoopTimes++;
            continue;
        }

        if ((nRet = recv(m_hSocket, szDataBuf, sizeof(szDataBuf) - 1,0)) < 0)
        {
            LogoDebug("SOCKET_ERROR, can not recv data. Error code = %d\n", strerror(errno));

            return false;
        }
        else if (0 == nRet)
        {
            LogoDebug("buf is empty\n");

            continue;
        }

        szDataBuf[nRet] = NULL;
        LogoDebug ("szDataBuf = [%s]", szDataBuf);

        scan = szDataBuf;
        while (nRet--)
        {
            m_ProtolProcess->TelentProtcol(m_hSocket,*scan++);
        }

        LogoDebug ("g_strRecvData = [%s]", g_strRecvData.c_str ());
        if (!bHaveSendUserName)
        {
            std::string::size_type ipos;
            /*
            if (std::string::npos != (ipos = g_strRecvData.find(m_strLogin)) &&
                g_strRecvData.size() == ipos + m_strLogin.size())
                */
            //if (std::string::npos != g_strRecvData.find(m_strLogin))
            if (NULL != strstr (g_strRecvData.c_str (), m_strLogin.c_str ()))
            {
                bHaveSendUserName = true;
                g_strRecvData.clear();

                strCmd = m_strUserName + m_szEnterKey;
                if (send(m_hSocket, strCmd.c_str(), strCmd.size(), 0) < 0)
                {
                    LogoDebug("SOCKET_ERROR, can not send data.\n");
                    return false;
                }
            }
        }
        else if (bHaveSendUserName && !bHaveSendPasswd)
        {
            std::string::size_type ipos;
            /*
            if (std::string::npos != (ipos = g_strRecvData.find(m_strPasswd)) &&
                g_strRecvData.size() == ipos + m_strPasswd.size())
            */
            //if (std::string::npos != g_strRecvData.find(m_strPasswd))
            if (NULL != strstr (g_strRecvData.c_str (), m_strPasswd.c_str ()))
            {
                bHaveSendPasswd = true;
                g_strRecvData.clear();

                strCmd = m_strUserPasswd + m_szEnterKey;
                if (send(m_hSocket, strCmd.c_str(), strCmd.size(), 0) < 0)
                {
                    LogoDebug("SOCKET_ERROR, can not send data.\n");

                    return false;
                }
            }
        }
        else if (bHaveSendUserName && bHaveSendPasswd)
        {
            std::string::size_type ipos;
            //if (std::string::npos != (ipos = g_strRecvData.find(m_strLastLogin)))
            if (NULL != strstr (g_strRecvData.c_str (), m_strLastLogin.c_str ()))
            {
                g_strRecvData.clear();

                break;
            }
            //else if (std::string::npos != (ipos = g_strRecvData.find(m_strLoginIncorrect)))
            else if (NULL != strstr (g_strRecvData.c_str (), m_strLoginIncorrect.c_str ()))
            {
                g_strRecvData.clear();

                LogoDebug("Login Incorrect, make sure username and userpasswd is right\n");
                fprintf(stderr, "Login Incorrect, make sure username and userpasswd is right\n");
                return false;
            }
        }
        else
        {
            LogoDebug("Can not login\n");
            return false;
        }
    }

    if (iLoopTimes == iMaxTimes)
    {
        fprintf(stderr, "Login Timeout.\n");
        LogoDebug("Login Over Time.\n");
        return false;
    }

    return true;
}

bool CTelnetManager::ExcuteShellCmd(const char *strShellCmd, std::string& strRes)
{
    if (!IsCanExcuteShellCmd)
    {
        LogoDebug("have not login success now, Can not excute shell cmd! \n");

        return 1;
    }

    char szCmd[256];
    strcpy (szCmd, strShellCmd);
    strcat (szCmd, m_szEnterKey);

    if (send(m_hSocket, szCmd, strlen (szCmd), 0) < 0)
    {
        LogoDebug("SOCKET_ERROR, can not send data.\n");

        return 1;
    }

    g_strRecvData.clear();
    strRes.clear ();

//    Start(strShellCmd);
    bool bRet = GetResult(strShellCmd);
   // bool bRet = Run();
    strRes = g_strRecvData;

    return bRet;

}

bool CTelnetManager::GetResult(const char *strCmd)
{
    int nRet;
    char* scan;
    fd_set myFDSet;
    struct timeval tmVal;
    tmVal.tv_sec = 0;
    tmVal.tv_usec = 800 * 1000;
    //int iLoopTimes = 0;
    //int iMaxTimes = 0;
    char szDataBuf[1024] = {'\0'};
    //memset(szDataBuf, 0, sizeof(szDataBuf));
    //bool bCleanCmdResult = false;

    LogoDebug ("cmd = [%s]", strCmd);
    while (1)
    {
        FD_ZERO(&myFDSet);
        FD_SET(m_hSocket, &myFDSet);
        if (select(m_hSocket + 1, &myFDSet, NULL, NULL, &tmVal) < 0)
        {
            LogoDebug("select failed.....\n");

            continue;
        }

        if (!FD_ISSET(m_hSocket, &myFDSet))
        {
            //enterLine
            usleep(800);
            //iLoopTimes++;
            //LogoDebug ("iLoopTimes = %d, iMaxTimes = %d", iLoopTimes, iMaxTimes);
            continue;
            /*
            if (iLoopTimes < iMaxTimes)
            {
                continue;
            }
            else
            {
                LogoDebug ("select timeout");
                return false;
            }*/
        }
        //iLoopTimes = 0;
        //iMaxTimes++;

        if ((nRet = recv(m_hSocket, szDataBuf, sizeof(szDataBuf) - 1,0)) < 0)
        {
            LogoDebug("SOCKET_ERROR, can not recv data.\n");

            return false;
        }
        else if (0 == nRet)
        {
            //exitLine
            return true;
        }

        szDataBuf[nRet] = '\0';
        scan = szDataBuf;
        while (nRet--)
        {
            m_ProtolProcess->TelentProtcol(m_hSocket, *scan++);
        }

        //LogoDebug ("szDataBuf = [%s]", szDataBuf);

        //setbuf(stdout, NULL);
        //fprintf (stdout, "%s", szDataBuf);
        //write (1, szDataBuf, strlen (szDataBuf));
        /*
        if (NULL != telnetPrint)
        {
            LogoDebug ("telnetPrint = %p", telnetPrint);
            int iLen = SendMessage(telnetPrint, WM_GETTEXTLENGTH,0, 0);
            SendMessage(telnetPrint, EM_SETSEL,iLen, iLen);
            //telnetPrint->SetSel (iLen, iLen);
            LogoDebug ("iLen = %d", iLen);
            SendMessage(telnetPrint, EM_REPLACESEL,(WPARAM)false , (LPARAM)szDataBuf);
            //telnetPrint->ReplaceSel (szDataBuf);
        }
        else
        {
            printf ("%s", szDataBuf);
            fflush(stdout);
        }
*/
        fprintf(stderr, szDataBuf);

        //if (strCmd == "exit")
        if (0 == strcmp (strCmd, "exit"))
        {
            if (strstr (g_strRecvData.c_str(), m_szEnterKey))
            {
                return true;
            }
        }
        else
        {
            if (std::string::npos != g_strRecvData.find(m_strFinish))
            {
                //接收完成
                return true;
            }
        }
    }
}


void CTelnetManager::setPromptStr (char *strLogin, char *strPasswd, char *strLastLogin, char *strLoginIncorrect)
{
    //if (NULL != strLogin)
    {
        m_strLogin = strLogin;
    }
    //if (NULL != strPasswd)
    {
        m_strPasswd = strPasswd;
    }
    //if (NULL != strLastLogin)
    {
        m_strLastLogin = strLastLogin;
    }
    //if (NULL != strLoginIncorrect)
    {
        m_strLoginIncorrect = strLoginIncorrect;
    }
    //if (NULL != strFinish)
    LogoDebug ("m_strLogin = [%s], m_strPasswd = [%s], m_strLastLogin = [%s], m_strLoginIncorrect = [%s], m_strFinish = [%s]",
    m_strLogin.c_str (), m_strPasswd.c_str (), m_strLastLogin.c_str (), m_strLoginIncorrect.c_str (), m_strFinish.c_str ());
}
bool CTelnetManager::DisConnect()
{
    if (enSocketNotConnect == m_iSocketState)
    {
        return true;
    }

    std::string strRes;
    if (!ExcuteShellCmd("exit", strRes))
    {
        LogoDebug("excute shell cmd [exit] failed.");
    }

    if (NULL == strstr (strRes.c_str(), "exit"))
    {
        LogoDebug("exit faled. strRes = [%s]", strRes.c_str());
    }

    m_iSocketState = enSocketNotConnect;
    if (close (m_hSocket) < 0)
    {
        LogoDebug ("false in closesocket(),error = [%s]", strerror (errno));
        return false;
    }
    else
    {
        m_hSocket = -1;
        return true;
    }
}
