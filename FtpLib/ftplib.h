#ifndef	_FTPLIB_H_
#define _FTPLIB_H_
#include  <stdio.h>
//#include <winsock2.h>

//typedef int in_port_t;
typedef int ssize_t;
///////////libftp.h

//#ifdef
//    #undef
//    #define  __declspec(dllexport)
//#else
//    #define  __declspec(dllimport)
//#endif


#define BINARY	1
#define ASCII	2
#define TENEX	3
#define EBCDIC	4

#define NORMAL		0
#define ABNORMAL 		1
#define ON              		1
#define OFF			0
#define ALTPORT		190

#ifndef	__CONST
	#define __CONST const		/* 自定义__CONST */
#endif

struct  ST_SPEED
{
    long	send_bytes;		/* total bytes transferred */
    long total_bytes;         /*总大小*/
    //float	kbs;			/* kilobytes per second */
    double costTime;  /*花费的时间difftime()获得*/
    ST_SPEED()
    {
        send_bytes = 0;
        total_bytes = 0;
        costTime = 0;
    }
};
typedef ST_SPEED SPEED;
 /*
Description of data base entry for a single service.
struct servent
{
  char *s_name;			// Official service name.
  char **s_aliases;		 //Alias list.
  int s_port;			// Port number.
  char *s_proto;		 //Protocol to use.
};
*/

struct  ST_FTPINFO
{
	FILE	*filedes;		/* data file descriptor */
	char	transf_type;		/* type of transfer (bin/ascii) */
	char	*ftp_msg; 		/* error status of function or reply
					   			string from server */
	int	transf_calc;		/* flag to turn on xfer rate calc */
	SPEED	speed;			/* xfer rate information */
	int	sockfd;			/* CNTRL connection socket id */
	int	datasd;			/* DATA connection socket id */
	int	reply;			/* return value of function or reply
					   code from server */
	int	debug;			/* flag to turn on debugging */
	//int	linger;			/* flag to turn on socket linger opt */
	int	connected;		/* flag to indicate the existance of
					   a control connection for the
					   session under question */
    ST_FTPINFO()
    {
        filedes = NULL;
        transf_type = -1;
        ftp_msg = NULL;
        transf_calc = 0;
        sockfd = -1;
        datasd = -1;
        connected = -1;
        debug = 1;
    }
	//struct	servent	*ftp_sp;	/* specific service entry */
} ;

typedef ST_FTPINFO FTPINFO ;


/*
#define LogoDebug \
     LOGDEBUG("\r\n[%s:%s:%d]", __FILE__, __FUNCTION__, __LINE__); LOGDEBUG

#define enterLine  LogoDebug("Enter");
#define exitLine  LogoDebug("Exit");
//extern FILE *fpLog;
*/

//void LogoDebug(const char *fmt, ...);
//void LOGDEBUG(const char *fmt, ...);
int ftp_accnt(FTPINFO *, __CONST char *);//d end
int ftp_ascii(FTPINFO *);//d end
int ftp_binary(FTPINFO *);//d end
int ftp_bye(FTPINFO *);//d end
int ftp_chdir(FTPINFO *, __CONST char *);//d end
int ftp_command(FTPINFO *, __CONST char *, __CONST char *); //d end
int ftp_dataconn(FTPINFO *, __CONST char *, __CONST char *, __CONST char *);
int ftp_del(FTPINFO *, __CONST char *);//d end
int ftp_dir(FTPINFO *, __CONST char *, __CONST char *);//d end
int ftp_ebcdic(FTPINFO *);
int ftp_getfile(FTPINFO *, __CONST char *, __CONST char *);
int ftp_idle(FTPINFO *, __CONST char *);
int ftp_initconn(FTPINFO *);
int ftp_login(FTPINFO *, __CONST char *, __CONST char *, __CONST char *, __CONST char *);//d end
int ftp_mkdir(FTPINFO *, __CONST char *);//d end
int ftp_passwd(FTPINFO *, __CONST char *);//d end
int ftp_setport( int );//d end
int ftp_prconnect(FTPINFO *, __CONST char *);//d end
int ftp_putfile(FTPINFO *, __CONST char *, __CONST char *);//d end
int ftp_appfile(FTPINFO *, __CONST char *, __CONST char *);
int ftp_pwd(FTPINFO *);//d end
int ftp_rmdir(FTPINFO *, __CONST char *);//d end
int ftp_settype(FTPINFO *, int);//d end
int ftp_site(FTPINFO *, __CONST char *);//d end
int ftp_tenex(FTPINFO *);
int ftp_user(FTPINFO *, __CONST char *);//d end


#define MAXLINE			1024 * 10

int tcp_connect_server(int *, char *, int);
void debug_print(const char *, ...);
void message_print(const char *, ...);
void signal_alarm(int);
ssize_t tcp_read_line(int, void *, size_t);
ssize_t tcp_read(int, char *);
ssize_t	tcp_write(int, void *, size_t);
ssize_t	tcp_readn(int, void *, size_t);
ssize_t file_read_line(FILE *, char *, int);

/////end of tcpbase.h





///////////ftpbase.h

//#include

#define		FTP_MESSAGE_LEN		1024



int ftp_response(FTPINFO *);
void ftp_set_timeout(FTPINFO *, int);
int ftp_check_response(FTPINFO *, char);
int ftp_pasv(FTPINFO *, char *, int *);
int ftp_get_data_sock(FTPINFO *);

/////end of ftpbase.h
void usage(char *progname);
//void check_n_close ( FTPINFO *ftpinfo, int status );
int check_n_close ( FTPINFO *ftpinfo, int status );
#endif
