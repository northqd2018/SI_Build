#include <ctype.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <memory.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdarg.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>
#include "ftplib.h"
#include "..\LogLib\log.h"
#define READLINE        256

#define ON              1
#define OFF             0

char    ftp_message[FTP_MESSAGE_LEN];
int   ftp_port;
int tcp_timeout;

const int SleepSize = 1024 * 1024 * 10;
/*
FILE *fpLog;

//void LogoDebug(const char *fmt, ...)
void LOGDEBUG(const char *fmt, ...)
{
    if (NULL != fpLog)
    {
        va_list vaList;
        va_start(vaList, fmt);
        vfprintf(fpLog, fmt, vaList);
        va_end(vaList);
        //putc('\n', fpLog);
        fflush(fpLog);
    }
}
*/

int ftp_getfile(FTPINFO *ftp_info, __CONST char *rem_path,
                    __CONST char *local_path)
{
    enterLine
    FILE    *fp_out;
    int     len;
    char    buffer[MAXLINE];
    char    file[MAXLINE], path[MAXLINE];
    int     i, n;
    char    *buf;

    if (rem_path == NULL)
    {
        LogoDebug("Must specify a file name to get");
        return -1;
    }

    if (ftp_info->transf_type == ASCII)
    {
        if (ftp_ascii(ftp_info) < 0)
            return -1;
    }
    else if (ftp_info->transf_type == BINARY)
    {
        if (ftp_binary(ftp_info) < 0)
            return -1;
    }
    else
    {
        LogoDebug ("pleas specify  transfer type");
        return -1;
    }

    if (ftp_get_data_sock(ftp_info) < 0)
        return -1;

    memset(file, 0, sizeof(file));
    for (n = 0, i = 0; n < strlen(rem_path); n++)
    {
        file[i++] = rem_path[n];
        if (rem_path[n] == '/')
        {
            memset(file, 0, sizeof(file));
            i = 0;
        }
    }

    LogoDebug ("file = %s", file);
    if (local_path == NULL)
        strcpy(path, file);
    else
        strcpy(path, local_path);

    if (ftp_command(ftp_info, rem_path, "retr") < 0)
    {
        LogoDebug("ftp command stor error!");
        return -1;
    }

    if (ftp_check_response(ftp_info, '1') < 0)
    {
        LogoDebug("ftp command stor response error!");
        //close(ftp_info->datasd);
        return -1;
    }

    if ((buf = strchr(ftp_info->ftp_msg, '(')))
    {
        buf++;
        ftp_info->speed.total_bytes = atoi(buf);
    }

    LogoDebug("ftp_info->speed.total_bytes = %d", ftp_info->speed.total_bytes);
    //获取当前时间
    time_t starttTime;
    time_t currentTime;
    time(&starttTime);
    ftp_info->speed.costTime = 0;
    ftp_info->speed.send_bytes = 0;
    if (ftp_info->transf_type == ASCII)
    {
        LogoDebug ("ascii mode transfer");
        fp_out = fopen(path, "w");
        if (fp_out == NULL)
        {
            LogoDebug("fp_out is NULL\n");
            return -1;
        }
        while ((len = tcp_read_line(ftp_info->datasd, buffer, MAXLINE - 1)) > 0)
        {
            //LogoDebug("Line: %s", buffer);
            if (strcmp(&buffer[len - 2], "\r\n") == 0)
            {
                len -= 2;
                buffer[len++] = '\n';
                buffer[len] = '\0';
            }
            if (fwrite(buffer, 1, len, fp_out) == -1)
            {
                LogoDebug("fwrite error");
                break;
            }

            //记录速度
            ftp_info->speed.send_bytes += len;
            time (&currentTime);
            ftp_info->speed.costTime = difftime(currentTime, starttTime);
        }
    }
    else
   {
        LogoDebug ("bin mode transfer");
        fp_out = fopen(path, "wb");
        if (fp_out == NULL)
        {
            LogoDebug("fp_out is NULL\n");
            return -1;
        }
        while ((len = tcp_readn(ftp_info->datasd, buffer, MAXLINE)) != 0)
        {
            //LogoDebug("%f, len = %d", ftp_info->speed.costTime, len);
            if (fwrite(buffer, 1, len, fp_out) == -1)
            {
                LogoDebug("fwrite error");
                break;
            }

            //记录速度
            ftp_info->speed.send_bytes += len;
            //LogoDebug("len = %d, send = %ld", len, ftp_info->speed.send_bytes);
            time (&currentTime);
            ftp_info->speed.costTime = currentTime - starttTime;
            if (0 == ftp_info->speed.send_bytes % SleepSize)
            {
                 usleep(20);
            }
        }
    }
    //time (&currentTime);
    //ftp_info->speed.costTime = difftime(currentTime, starttTime);
    LogoDebug ("use time = %f", ftp_info->speed.costTime);
    fclose(fp_out);

    if (close(ftp_info->datasd) < 0)
        perror("close");

    exitLine
    return (ftp_check_response(ftp_info, '2'));
}

int ftp_putfile(FTPINFO *ftp_info, __CONST char *rem_path,
                    __CONST char *local_path)
{
    FILE    *fp_in;
    int     len;
    char    buffer[MAXLINE];
    char    file[MAXLINE], path[MAXLINE];
    int     i, n;

    if (local_path == NULL)
    {
        LogoDebug("Must specify a file name to send");
        return -1;
    }

   if (ftp_info->transf_type == ASCII)
    {
        if (ftp_ascii(ftp_info) < 0)
            return -1;
    }
    else if (ftp_info->transf_type == BINARY)
    {
        if (ftp_binary(ftp_info) < 0)
            return -1;
    }
    else
    {
        LogoDebug ("pleas specify  transfer type");
        return -1;
    }

    if (ftp_get_data_sock(ftp_info) < 0)
    {
        exitLine
        return -1;
    }

    memset(file, 0, sizeof(file));
    for (n = 0, i = 0; n < strlen(local_path); n++)
    {
        file[i++] = local_path[n];
        if (local_path[n] == '\\')
        {
            memset(file, 0, sizeof(file));
            i = 0;
        }
    }

    LogoDebug ("file name = [%s]", file);

    if (rem_path == NULL)
        strcpy(path, file);
    else
        strcpy(path, rem_path);

    if (ftp_command(ftp_info, path, "STOR") < 0)
    {
        LogoDebug("ftp command stor error!");
        return -1;
    }

    if (ftp_check_response(ftp_info, '1') < 0)
    {
        LogoDebug("ftp command stor response error!");
        //close(ftp_info->datasd);
        return -1;
    }

    //获取当前时间
    time_t starttTime;
    time_t currentTime;
    time(&starttTime);
    ftp_info->speed.costTime = 0;
    ftp_info->speed.send_bytes = 0;

    if (ftp_info->transf_type == ASCII)
    {
        LogoDebug ("ascii mode transfer");
        fp_in = fopen(local_path, "r");
        if (fp_in == NULL)
        {
             LogoDebug ("fopen [%s] error", local_path);
             return -1;
        }

        while (!feof (fp_in))
        {
            len = fread( buffer, 1, MAXLINE - 2, fp_in);

            if(ferror(fp_in))
            {
                 LogoDebug( "Read error" );
                 break;
            }

            if (buffer[len-1] == '\n')
            {
                buffer[len-1] = '\r';
                buffer[len++] = '\n';
                buffer[len] = '\0';
            }

            if (tcp_write(ftp_info->datasd, buffer, len) == -1)
            {
                LogoDebug("tcp_write error");
                break;
            }

            //记录速度
            ftp_info->speed.send_bytes += len;
            time (&currentTime);
            ftp_info->speed.costTime = currentTime - starttTime;
        }
    }
    else
    {
         LogoDebug ("bin mode transfer");
         fp_in = fopen(local_path, "rb");
         if (fp_in == NULL)
         {
             LogoDebug ("fopen [%s] error", local_path);
             return -1;
         }
       //  LogoDebug("buffer = %0x", buffer);
         while (!feof (fp_in))
        {
            //len = fread( buffer, 1, MAXLINE * 2 - 1, fp_in);
            len = fread( buffer, 1, MAXLINE, fp_in);
            //LogoDebug ("buffer len = %d", len);
            //if(ferror(fp_in))
            //{
           //      LogoDebug( "Read error" );
            //     break;
            //}
            if (tcp_write(ftp_info->datasd, buffer, len) == -1)
            {
                LogoDebug("tcp_write error");
                break;
            }

            //记录速度
            ftp_info->speed.send_bytes += len;
            time (&currentTime);
            ftp_info->speed.costTime = currentTime - starttTime;
            if (0 == ftp_info->speed.send_bytes % SleepSize)
           {
                usleep(20);
           }
        }

    }

    LogoDebug ("use time = %f", ftp_info->speed.costTime);
    if (0 == fclose(fp_in))
    {
        LogoDebug ("fclose successfully");
    }
    else
    {
        LogoDebug ("fclose false");
        return -1;
    }

    if (close(ftp_info->datasd) < 0)
        perror("close");

    //return 1;
    return (ftp_check_response(ftp_info, '2'));
}

int ftp_dir(FTPINFO *ftp_info, __CONST char *rem_path,
                __CONST char *local_path)
{
    FILE    *fp_out;
    char    buffer[MAXLINE];
    int     len;

    assert(rem_path != NULL);

    if (ftp_info->transf_type == BINARY)
        if (ftp_ascii(ftp_info) < 0)
            return -1;
    if (ftp_get_data_sock(ftp_info) < 0)
        return -1;
    if (ftp_command(ftp_info, rem_path, "list") < 0)
    {
        LogoDebug("ftp command list error!");
        return -1;
    }
    if (ftp_check_response(ftp_info, '1') < 0)
    {
        LogoDebug("ftp command list error!");
        //close(ftp_info->datasd);
        return -1;
    }
    if (local_path == NULL)
        fp_out = stdout;
    else
    {
        fp_out = fopen(local_path, "w");
        if (fp_out == NULL)
        {
            //perror(local_path);
            LogoDebug ("fopen [%s] failse" ,local_path);
            fp_out = stdout;
        }
    }
    while ((len = tcp_readn(ftp_info->datasd, buffer, MAXLINE)) > 0)
        fwrite(buffer, 1, len, fp_out);
    if (local_path != NULL)
        fclose(fp_out);
    //close(ftp_info->datasd);

    return (ftp_check_response(ftp_info, '2'));
}

int ftp_site(FTPINFO *ftp_info, __CONST char *cmd)
{
    if (ftp_command(ftp_info, cmd, "site") < 0)
    {
        LogoDebug("ftp command site error!");
        return -1;
    }
    return (ftp_check_response(ftp_info, '2'));
}

int ftp_mkdir(FTPINFO *ftp_info, __CONST char *path)
{
    if (ftp_command(ftp_info, path, "mkd") < 0)
    {
        LogoDebug("ftp command cwd error!");
        return -1;
    }
    return (ftp_check_response(ftp_info, '2'));
}

int ftp_settype(FTPINFO *ftp_info, int type)
{
    enterLine
    if (ASCII == type)
        return (ftp_ascii(ftp_info));
    if (BINARY == type)
        return (ftp_binary(ftp_info));

    LogoDebug("Type should be one of ASCII or BINARY.");
    return -1;
}

int ftp_rmdir(FTPINFO *ftp_info, __CONST char *path)
{
    if (ftp_command(ftp_info, path, "rmd") < 0)
    {
        LogoDebug("ftp command cwd error!");
        return -1;
    }
    return (ftp_check_response(ftp_info, '2'));
}

int ftp_pwd(FTPINFO *ftp_info)
{
    if (ftp_command(ftp_info, "", "pwd") < 0)
    {
        LogoDebug("ftp command quit error!");
        return -1;
    }
    return (ftp_check_response(ftp_info, '2'));
}

int ftp_del(FTPINFO *ftp_info, __CONST char *file)
{
    if (ftp_command(ftp_info, file, "dele") < 0)
    {
        LogoDebug("ftp command cwd error!");
        return -1;
    }
    return (ftp_check_response(ftp_info,  '2'));
}

int ftp_chdir(FTPINFO *ftp_info, __CONST char *rempath)
{
    if (ftp_command(ftp_info, rempath, "cwd") < 0)
    {
        LogoDebug("ftp command cwd error!");
        return -1;
    }
    return (ftp_check_response(ftp_info,  '2'));
}

int ftp_ascii(FTPINFO *ftp_info)
{
    if (ftp_command(ftp_info, "A", "TYPE") < 0)
    {
        LogoDebug("ftp command type a error!");
        return -1;
    }
    if (ftp_check_response(ftp_info,  '2') < 0)
    {
        LogoDebug("read ftp command type a response error!");
        return -1;
    }
    ftp_info->transf_type = ASCII;

    return 1;
}

int ftp_binary(FTPINFO *ftp_info)
{
    if (ftp_command(ftp_info, "I", "TYPE") < 0)
    {
        LogoDebug("ftp command type i error!");
        return -1;
    }
    if (ftp_check_response(ftp_info,  '2') < 0)
    {
        LogoDebug("read ftp command type i response error!");
        return -1;
    }
    ftp_info->transf_type = BINARY;

    return 1;
}

int ftp_bye(FTPINFO *ftp_info)
{
    if (ftp_command(ftp_info, "", "quit") < 0)
    {
        LogoDebug("ftp command quit error!");
        return -1;
    }
    return (ftp_check_response(ftp_info,  '2'));
}

int ftp_accnt(FTPINFO *ftp_info, __CONST char *account)
{
    return 1;
}

int ftp_login(FTPINFO *ftp_info, __CONST char *remhost,
                __CONST char *user, __CONST char *passwd, __CONST char *account)
{
    enterLine
    if (ftp_prconnect(ftp_info, remhost) < 0)
        return -1;
    if (ftp_user(ftp_info, user) < 0)
        return -1;
    if (ftp_passwd(ftp_info, passwd) < 0)
        return -1;

    return 1;
    //exitLine
    //return (ftp_accnt(ftp_info, account));
}

int ftp_passwd(FTPINFO *ftp_info, __CONST char *passwd)
{
    if (ftp_command(ftp_info, passwd, "pass") < 0)
    {
        LogoDebug("ftp command user error!");
        return -1;
    }
    return (ftp_check_response(ftp_info,  '2'));
}

int ftp_user(FTPINFO *ftp_info, __CONST char *user)
{
    if (ftp_command(ftp_info, user, "user") < 0)
    {
        LogoDebug("ftp command user error!");
        return -1;
    }
    return (ftp_check_response(ftp_info,  '3'));
}

int ftp_command(FTPINFO *ftp_info, __CONST char *opt_info,
                 __CONST char *action)
{
    char    command[256];

    assert(action != NULL);
    assert(opt_info != NULL);

    if (ftp_info->connected != 1)
    {
        LogoDebug("The server is not connected");
        return -1;
    }

    memset(command, 0, sizeof(command));
    //printf("opt_info:%s\n",opt_info);
    if (strlen(opt_info) == 0)
        sprintf(command, "%s\r\n", action);
    else
        sprintf(command, "%s %s\r\n", action, opt_info);

    LogoDebug("Send command: %s", command);
    //printf("Send command: %s", command);
    if (tcp_write(ftp_info->sockfd, command, strlen(command)) < 0)
    {
        LogoDebug("tcp_write command error!");
        return -1;
    }

    return 1;
}

int ftp_prconnect(FTPINFO *ftp_info, __CONST char *host)
{
    enterLine
    ftp_info->connected = 1;

    LogoDebug("Start to connect server: %s", host);

    if (tcp_connect_server(&(ftp_info->sockfd), (char *)host, ftp_port) < 0)
    {
        LogoDebug("Can't connect to server!");
        return -1;
    }
    ftp_info->connected = 1;
    ftp_info->ftp_msg = ftp_message;

    if (ftp_check_response(ftp_info,  '2') < 0)
    {
        //close(ftp_info->connected);
        return -1;
    }

    LogoDebug("Connect Success!\n");

    return 1;
}


int ftp_setport(int port)
{
    /*
    struct servent *se=NULL;
    printf("enter ftp_setport\n");
    if (port == 0)
    {
        printf("enter port==0\n");
        if ((se = getservbyname("ftp", "tcp")) == NULL)
        {
            printf("before perror(\"getservbyname\")\n");
            perror("getservbyname");
            return -1;
        }
        printf("before htons\n");
        ftp_port = htons(se->s_port);
    } else
        ftp_port = port;
    */
   // ftp_port = 21;
    ftp_port = port;
    return 1;
}


/////end of libftp.c


//////////////////////////////////////////////////////

///////////tcpbase.c

ssize_t file_read_line(FILE *fp, char *buffer, int max)
{
    int     n, rc;
    char    c, *ptr;

    ptr = buffer;

    for (n = 1; n < max; n++)
    {
        if ((rc = fread(&c, 1, 1, fp)) == 1)
        {
            *ptr++ = c;
            if (c == '\n')
                break;  /* newline is stored, like fgets() */
        }
        else
            if (rc == 0)
            {
                if (n == 1)
                    return 0;   /* EOF, no data read */
                else
                    break;      /* EOF, some data was read */
            }
            else
                return -1;      /* error, errno set by read() */
    }

    *ptr = 0;   /* null terminate like fgets() */

    return n;
}

ssize_t tcp_write(int fd, void *vptr, size_t n)
    /* Write "n" bytes to a descriptor. */
{
    //enterLine
    size_t      nleft;
    ssize_t     nwritten;
    const char  *ptr;

    ptr = (char*)vptr;
    nleft = n;
    /*
    signal(SIGALRM, signal_alarm);
    if (alarm(tcp_timeout) != 0)
        LogoDebug("connect_timeo: alarm was already set");
    */
    while (nleft > 0)
    {
        //if ((nwritten = write(fd, ptr, nleft)) <= 0)
        if ((nwritten = send (fd, ptr, nleft, 0)) <= 0)
        {
            //errno = 0;
            LogoDebug("errno = %d", errno);
            switch(errno)
           {
              case EBADF:
                 LogoDebug("Bad file descriptor!");
                 break;
              case ENOSPC:
                 LogoDebug("No space left on device!");
                 break;
              case EINVAL:
                 LogoDebug("Invalid parameter: buffer was NULL!");
                 break;
              default:
                 // An unrelated error occured
                 LogoDebug("Unexpected error!");
                 break;
           }
            errno = 0;

        }

        nleft -= nwritten;
        ptr   += nwritten;
    }

    //alarm(0);                 /* turn off the alarm */

    //LogoDebug("Write buffer len: %d", n);
    //exitLine
    //LogoDebug ("tcp_write %d byte", n);
    return n;
}

ssize_t tcp_readn(int fd, void *vptr, size_t n)
/* Read "n" bytes from a descriptor. */
{
    size_t  nleft;
    ssize_t nread;
    char    *ptr;

    ptr = (char*)vptr;
    nleft = n;
    /*
    signal(SIGALRM, signal_alarm);
    if (alarm(tcp_timeout) != 0)
        LogoDebug("connect_timeo: alarm was already set");
    */
    while (nleft > 0)
    {
        //enterLine
        //if ( (nread = read(fd, ptr, nleft)) < 0)
        if ( (nread = recv(fd, ptr, nleft, 0)) < 0)
        {
            if (errno == EINTR)
                nread = 0;      /* and call read() again */
            else
                return(-1);
        }
        else if (nread == 0)
        {
            break;              /* EOF */
        }

        nleft -= nread;
        ptr   += nread;
    }
    /*
    alarm(0);
    */
    return(n - nleft);      /* return >= 0 */
}

ssize_t tcp_read_line(int fd, void *vptr, size_t maxlen)
{
    enterLine
    int     n, rc;
    char    c, *ptr;

    ptr = (char*)vptr;
    /*
    signal(SIGALRM, signal_alarm);
    if (alarm(tcp_timeout) != 0)
        LogoDebug("connect_timeo: alarm was already set");
    */
    for (n = 1; n < (int)maxlen; n++)
    {
        if ((rc = tcp_read(fd, &c)) == 1)
        {
            *ptr++ = c;
            if (c == '\n')
                break;  /* newline is stored, like fgets() */
        }
        else
            if (rc == 0)
            {
                if (n == 1)
                {
                    exitLine
                    return 0;   /* EOF, no data read */
                }
                else
                    break;      /* EOF, some data was read */
            }
            else
            {
                exitLine
                return -1;      /* error, errno set by read() */
            }
    }
    /*
    alarm(0);
    */
    *ptr = 0;   /* null terminate like fgets() */
    exitLine
    return n;
}

ssize_t tcp_read(int fd, char *ptr)
{
//    enterLine
    static int  read_cnt = 0;
    static char *read_ptr;
    static char read_buf[MAXLINE];
    //FILE *file = _fdopen(fd, "rb");
    //long  lfd = _get_osfhandle(fd);
    //LogoDebug("lfd = %ld", lfd);
    if (read_cnt <= 0)
    {
        do
        {
            //LogoDebug("fd = %d\n", fd);
            //if ( (read_cnt = read(fd, read_buf, sizeof(read_buf))) < 0)
            enterLine
            if ( (read_cnt = recv (fd, read_buf, sizeof(read_buf), 0)) < 0)
            //if ( (read_cnt = fread(read_buf, sizeof(read_buf), MAXLINE, file)) < 0)
            {
                //enterLine
                if (errno == EINTR)
                    continue;
                exitLine
                return(-1);
            }
            else if (read_cnt == 0)
            {
                //exitLine
                return(0);
            }
            read_ptr = read_buf;
       }while(0);
    }

    read_cnt--;
    *ptr = *read_ptr++;
    //exitLine
    return 1;
}

int tcp_connect_server(int *sock, char *server, int port)
{
    int     sockfd;
    struct  sockaddr_in servaddr;

    assert(server != NULL);
    //sockfd = socket(AF_INET, SOCK_STREAM, 0);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        perror ("socket");
        LogoDebug("sockfd == INVALID_SOCKET, err = [%s]", strerror (errno));
        return -1;
    }
    memset((void *)&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port   = htons(port);
    //if (inet_pton(AF_INET, server, &servaddr.sin_addr) <= 0)
    if ((servaddr.sin_addr.s_addr = inet_addr(server)) < 0)
    {
        struct hostent* phost;
        phost = (struct hostent*)gethostbyname(server);
        if(phost==NULL){
            LogoDebug("Init socket s_addr error!");
            return -1;
        }
        servaddr.sin_addr.s_addr =((struct in_addr*)phost->h_addr)->s_addr;
    }
    /*
    signal(SIGALRM, signal_alarm);
    if (alarm(tcp_timeout) != 0)
        LogoDebug("connect_timeo: alarm was already set");
    */
    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        //close(sockfd);
        //if (errno == EINTR)
          //  errno = ETIMEDOUT;
        LogoDebug( "Failed to connect.\n" );
        //WSACleanup();
        *sock = -1;
        return -1;

    }

    //alarm(0);                 /* turn off the alarm */

    *sock = sockfd;
    exitLine
    return 1;
}

void signal_alarm(int signo)
{
    return;     /* just interrupt the connect() */
}

/////end of tcpbase.c


/////////////////////////////////////////////////


///////////ftpbase.c

int ftp_get_data_sock(FTPINFO *ftp_info)
{
    enterLine
    char    data_host[256];
    int     data_port;

    if (ftp_pasv(ftp_info, data_host, &data_port) < 0)
    {
        LogoDebug("ftp pasv error!");
        return -1;
    }

    LogoDebug("Host: %s; Port: %d", data_host, data_port);

    if (tcp_connect_server(&(ftp_info->datasd), (char *)data_host,
                            data_port) < 0)
    {
        LogoDebug("Can't connect to data server!");
        return -1;
    }

    exitLine
    return 1;
}

int ftp_pasv(FTPINFO *ftp_info, char *dhost, int *dport)
{
    enterLine
    char    buffer[MAXLINE];
    unsigned char   *p;
    unsigned char addr[6];
    int     i;

    assert(dhost != NULL);

    if (ftp_command(ftp_info, "", "pasv") < 0)
    {
        LogoDebug("ftp command pasv error!");
        return -1;
    }
    if (ftp_check_response(ftp_info, '2') < 0)
    {
        LogoDebug("check ftp command pasv response error!");
        return -1;
    }
    strcpy(buffer, ftp_info->ftp_msg);
    p = (unsigned char *)buffer;
    for (p += 4; *p && !isdigit(*p); p++);

    if (!*p)
    {
        exitLine
        return -1;
    }

    for (i = 0; i < 6; i++)
    {
        addr[i] = 0;
        for (; isdigit(*p); p++)
        {
            addr[i] = (*p - '0') + 10 * addr[i];
        }

        if (*p == ',')
        {
            p++;
        }
        else if (i < 5)
        {
            exitLine
            return -1;
        }
    }

    sprintf(dhost, "%d.%d.%d.%d", addr[0], addr[1], addr[2], addr[3]);
    *dport = (addr[4] << 8) + addr[5];

    exitLine
    return 1;
}

void ftp_set_timeout(FTPINFO *ftp_info, int sec)
{
    tcp_timeout = sec;
    return;
}

int ftp_read_response(FTPINFO *ftp_info)
{
    enterLine
    char match[5];
   //LogoDebug("Sock fd: %d", ftp_info->sockfd);
    if (tcp_read_line(ftp_info->sockfd, ftp_info->ftp_msg, MAXLINE) == -1)
    {
        LogoDebug("Control socket read failed");
        return -1;
    }

    if (ftp_info->debug == 1)
        LogoDebug("%s", ftp_info->ftp_msg);
    if (ftp_info->ftp_msg[3] == '-')
    {
        strncpy(match, ftp_info->ftp_msg, 3);
        match[3] = ' ';
        match[4] = '\0';
        do
        {
            if (tcp_read_line(ftp_info->sockfd, ftp_info->ftp_msg,
                            MAXLINE) == -1)
            {
                LogoDebug("Control socket read failed");
                return -1;
            }
            if (ftp_info->debug == 1)
                LogoDebug("%s", ftp_info->ftp_msg);
        }
        while (strncmp(ftp_info->ftp_msg, match, 4));
    }

    exitLine
    return 1;
}

int ftp_check_response(FTPINFO *ftp_info, char c)
{
    enterLine
    if (ftp_read_response(ftp_info) < 0)
    {
        exitLine
        return -1;
    }

    if (ftp_info->ftp_msg[0] == c)
    {
        exitLine
        return 1;
    }
    exitLine
    return -1;
}



/////end of ftpbase.c
void usage(char *progname)
{
        printf("usage: %s <remhost>\n", progname);
        exit (1);
}

int check_n_close ( FTPINFO *ftpinfo, int status )
{
    /*
    if (ftpinfo -> sockfd >= 0)
        close (ftpinfo -> sockfd);
    */
    //enterLine
    if (status == ABNORMAL)
    {
        LogoDebug("error: %s\n", ftpinfo -> ftp_msg);
    }
    else
    {
        LogoDebug("success: %s\n", ftpinfo -> ftp_msg);
    }

   LogoDebug("final reply from server: %d\n", ftpinfo -> reply);
   fflush ( stdout );
   return (status);
}
