/* simple exec example */
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <libssh/sftp.h>
#include <libssh/libssh.h>
#include <libssh/session.h>
#include "examples_common.h"
#include "../LogLib/log.h"
#include "../ConfigLib/Config.h"
#include "../TelnetLib/telnetmanager.h"


void usage(int argc, char **argv)
{
    fprintf(stderr, "you input is:\n");
    for (int i = 0; i < argc; i++)
    {
        fprintf(stderr, "%s  ", argv[i]);
    }
    fprintf(stderr, "\nusage:\n"
            "%s -f filename: upload file\n"
            "%s -a: upload all the file you save\n"
            "%s -s filename: save the filename in project.txt\n"
            "%s -p project: specify the project name(must be specify)\n"
            , argv[0], argv[0], argv[0], argv[0]);
    fflush(stdout);
    getchar ();
    exit (-1);
}


void processParam(int argc, char **argv, struct ST_ArgvOpt *pParam)
{
    for (int i = 0; i < argc; ++i)
    {
        if (0 == strcmp(argv[i], "-s"))
        {
            //需要保存文件
            ++i;
            if (i >= argc)
            {
                usage(argc, argv);
            }
            strcpy (pParam->szSaveFile, argv[i]);
            continue;
        }

        if (0 == strcmp(argv[i], "-a"))
        {
            //上传所有
            pParam->bUploadAll = true;
            ++i;
            continue;
        }

        if (0 == strcmp(argv[i], "-f"))
        {
            //需要上传文件
            ++i;
            if (i >= argc)
            {
                usage(argc, argv);
            }
            strcpy (pParam->szUploadFile, argv[i]);
            continue;
        }

        if (0 == strcmp(argv[i], "-p"))
        {
            //记录工程的名字
            ++i;
            if (i >= argc)
            {
                usage(argc, argv);
            }
            strcpy (pParam->szProjectName, argv[i]);
            setProjectPath (pParam->szProjectName);
            continue;
        }

    }

    if ('\0' == pParam->szProjectName[0])
    {
        //必须制定工程的名字
        LogoDebug("must be specify the project name (-p project)\n");
        usage(argc, argv);
    }
}


bool saveFile(struct ST_ArgvOpt *pParam)
{
    FILE * fFileList;
    char buf[1024];
    fFileList  = fopen(getSaveFile (pParam->szProjectName), "a+");

    if (NULL == fFileList)
    {
        LogoDebug ("open %s false!\n", getSaveFile (pParam->szProjectName));
        fprintf(stderr, "open %s false!\n", getSaveFile (pParam->szProjectName));
        return false;
    }

    if (fseek (fFileList, 0, SEEK_SET) < 0)
    {
        LogoDebug ("fseek [%s] at 0 of SEEK_SET failse", getSaveFile (pParam->szProjectName));
        fclose (fFileList);
        return false;
    }

    while (0 == feof(fFileList))
    {
        //检查文件是否已经在保存列表中
        char *p = fgets(buf, sizeof (buf), fFileList);
//        LogoDebug ("buf = [%s]", buf);
        if (NULL == p)
        {
            LogoDebug("can't find [%s] in [%s], need save.\n", pParam->szSaveFile, getSaveFile (pParam->szProjectName));
            break;
        }
        if (NULL != strstr (buf, pParam->szSaveFile))
        {
            //文件已经存在文件列表中，不需要保存
            LogoDebug ("find %s in %s. not need to save!!!", pParam->szSaveFile, getSaveFile (pParam->szProjectName));
            fclose (fFileList);
            return true;
        }
    }

    if (fseek (fFileList, 0, SEEK_END) < 0)
    {
        LogoDebug ("fseek [%s] at 0 of SEEK_SET failse", getSaveFile (pParam->szProjectName));
        fclose (fFileList);
        return false;
    }

    fputs (pParam->szSaveFile, fFileList);
    fputs ("\n", fFileList);
    fclose (fFileList);
    return true;
}


bool upload (sftp_session sftp, char *filename)
{
    ST_File *p = g_File;
    char strPath[Max_PathLen + 1] = {'\0'};
    char strName[Max_FileNameLen + 1] = {'\0'};
    //enterLine;
#if 0
    if (!splitFilePath(filename, strPath, strName, '\\'))
    {
        return false;
    }
    do
    {
        //搜索上传列表
        if (NULL != strstr (p->strPath[EN_LocalPath], strPath) && NULL !=  strstr (p->szFileList, strName))
        {
            //已经找到上传路径
            //strServerPath = p->strPath[EN_ServerPath];
            //strLocalPath = p->strPath[EN_LocalPath];
            break;
        }
        p = p->next;
    }while (NULL != p);

    if (NULL == p)
    {
        //文件不在上传列表中
        LogoDebug ("can't find %s in config file \n", filename);
        fprintf(stderr, "can't find %s in config file \n", filename);
        fflush(stdout);
        getchar ();
        return false;
    }
    else
#endif
    {
        //检查文件是否是写保护，防止上传的文件不是最新的。(没有check out)
        int iResult = access(filename, R_OK | W_OK | F_OK);
        if (0 != iResult)
        {

        }

        //开始上传
#if 0
        char rem_file[1024 + 1];
        int iLoopTime = 0;
        strcpy (rem_file, p->strPath[EN_ServerPath]);
        strcat (rem_file, strName);
#endif
        char* rem_file;
        do
        {
            rem_file = getRemoteFilename (filename, p->strPath[EN_ServerPath], p->strPath[EN_LocalPath]);
            p = p->next;
        }while (NULL == rem_file && NULL != p);

        if (NULL == rem_file)
        {
            fprintf(stderr, "can't fine file in config [%s]\n", filename);
            return false;
        }
        //开始上传
        sftp_file to = sftp_open (sftp, rem_file, O_RDWR | O_CREAT | O_TRUNC, 0755);
        if (NULL == to)
        {
            fprintf(stderr, "sftp_open error [%s], rem_file = [%s]\n", ssh_get_error(sftp->session), rem_file);
            if (strstr (ssh_get_error(sftp->session), "No such file"))
            {
                //目录不存在则创建
                ssh_channel_request_exec(sftp->channel, "");
            }
            return -1;
        }

        FILE *from = fopen (filename, "r");
        if (NULL == from)
        {
            perror ("fopen");
            sftp_close(to);
            return -1;
        }
        fprintf(stderr, "uploadding [%s]\n", filename);
        int nbytes = -1;
        char buffer[4096];
        while((nbytes=fread (buffer, 1, sizeof (buffer), from)) > 0){
            if (nbytes != sftp_write (to, buffer, nbytes))
            {
                    fprintf(stderr, "sftp_write error [%s]\n", ssh_get_error(sftp->session));
            }
        }
//        fprintf(stderr, "upload [%s]finish\n", filename);
        fclose (from);
        sftp_close(to);
    }
}


void uploadFileList(sftp_session sftp, struct ST_ArgvOpt *pParam)
{
    char filename[1024] = {'\0'};
    FILE * fFileList;
    char serverPath[512] = {'\0'};

    fFileList  = fopen(getSaveFile (pParam->szProjectName), "r");
    if (NULL == fFileList)
    {
        LogoDebug("fFileList = NULL\n");
        fprintf(stderr, "open %s false!\n", getSaveFile (pParam->szProjectName));
        fflush(stdout);
        getchar ();
        exit (-1);
    }

    while (0 == feof(fFileList))
    {
        //读取文件
        if (NULL == fgets(filename, sizeof (filename), fFileList))
        {
            LogoDebug("fgets= NULL \n");
            break;
        }

        int iTempLen = strlen(filename) - 1;
        while (iTempLen >= 0)
        {
            if (('\n' == filename[iTempLen]) || ('\r' == filename[iTempLen]))
            {
                filename[iTempLen] = '\0';
                iTempLen--;
                continue;
            }
            break;
        }
        upload(sftp, filename);
//        LogoDebug ("filename = %s\n", filename);
    }
    fclose (fFileList);

    //清空文件内容
    //fFileList  = fopen("saveFile.txt", "w");
    fFileList  = fopen(getSaveFile (pParam->szProjectName), "w");
    if (NULL == fFileList)
    {
        LogoDebug("fFileList = NULL\n");
        fprintf(stderr, "open %s false!\n", getSaveFile (pParam->szProjectName));
        fflush(stdout);
        getchar ();
        exit (-1);
    }
    fclose (fFileList);
}


void checkDir ()
{
    if (access (getFilePath ("", SAVE_PATH), R_OK | W_OK) < 0)
    {
        //保存文件目录不存在
        if (mkdir(getFilePath ("", SAVE_PATH), R_OK | W_OK) < 0)
        {
            perror ("mkdir SAVE_PATH");
        }
    }

    if (access (getFilePath ("", CONFIG_PATH), R_OK | W_OK) < 0)
    {
        //配置文件目录不存在
        if (mkdir(getFilePath ("", CONFIG_PATH), R_OK | W_OK) < 0)
        {
            perror ("mkdir CONFIG_PATH");
        }
    }

    if (access (getFilePath ("", LOG_PATH), R_OK | W_OK) < 0)
    {
        //日志文件目录不存在
        if (mkdir(getFilePath ("", LOG_PATH), R_OK | W_OK) < 0)
        {
            perror ("mkdir LOG_PATH");
        }
    }
}

int ExecuteCMDTelnet (const char* user, const char* passwd, const char* ip,
                      const char* cmd, const char* loginPrompt,
                      const char* passwdPrompt, const char* lastLoginPrompt,
                      const char* loginIncorrectPrompt)
{
        CTelnetManager telnet;
        if (!telnet.Connect(user, passwd, ip))
        {
            printf ("connect server [%s], use the username [%s], password [%s] false\n",
                ip, user, passwd);
            fflush (stdout);
            return -1;
        }

        telnet.setPromptStr(loginPrompt, passwdPrompt, lastLoginPrompt, loginIncorrectPrompt);

        std::string strBuildRes;
        std::string strTempRes;

        telnet.ExcuteShellCmd (cmd,  strBuildRes);
        telnet.DisConnect();
}


int ExecuteCMDSsh (const char* user, const char* passwd, const char* ip, ssh_session session)
{

}


int main(int argc, char **argv) {

    char* p;
//    if((p=getenv("CYGWIN")))
//        printf("CYGWIN=[%s]\n",p);
//    setenv ("CYGWIN", "nodosfilewarning", 0);
//    if((p=getenv("CYGWIN")))
//        printf("CYGWIN=[%s]\n",p);

    //处理命令行选项
    ST_ArgvOpt opt;
    processParam(argc, argv, &opt);

    //检查目录是否存在
    checkDir ();
    //初始化日志
    if (initLog ("SIToollog.txt") < 0)
    {
        return 1;
    }

    if ('\0' != opt.szSaveFile[0])
    {
        saveFile(&opt);
    }

    if ('\0' != opt.szUploadFile[0] || opt.bUploadAll)
    {
        if (!readCfg(getConfigFile (opt.szProjectName), true))
        {
            fprintf(stderr, "false in read config file[%s]\n", getConfigFile (opt.szProjectName));
            LogoDebug ("false in read config file[%s]\n", getConfigFile (opt.szProjectName));
            fflush (stdout);
            getchar ();
            exit (-1);
        }

        ssh_session session = NULL;
        ssh_channel channel = NULL;
        sftp_session sftp = NULL;
        session = connect_ssh (g_Server->strIP, g_Server->strUserName, 0);
        if (NULL == session) {
            ssh_finalize();
            fprintf(stderr, "connect_ssh failse.IP = [%s].username = [%s].password = [%s]\n",
                            g_Server->strIP, g_Server->strUserName, g_Server->strPassword);
            return 1;
        }

        sftp = sftp_new(session);
        if (NULL != sftp)
        {
            sftp_init(sftp);

            if ('\0' != opt.szUploadFile[0])
            {
                fprintf(stderr, "upload[%s]\n", opt.szUploadFile);
                upload(sftp, opt.szUploadFile);
            }

            if (opt.bUploadAll)
            {
                uploadFileList(sftp, &opt);
            }
        }
        else
        {
            LogoDebug ("sftp_new failse[%s]", ssh_get_error(session));
        }

        channel = ssh_channel_new(session);
        if (NULL != (channel = ssh_channel_new(session)))
        {
            ssh_channel_open_session(channel);
            ssh_channel_request_exec(channel, g_cmd->szBuildCMD);
            ssh_channel_send_eof(channel);
            ssh_channel_close(channel);
            ssh_channel_free(channel);
        }
        else
        {
            LogoDebug ("ssh_channel_new failse[%s]", ssh_get_error(session));
        }

        ssh_disconnect(session);
        ssh_free(session);
        ssh_finalize();
    }
    return 0;
}
