/* simple exec example */
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <libssh/sftp.h>
#include <libssh/libssh.h>
#include <libssh/session.h>
#include "examples_common.h"
#include "../LogLib/log.h"
#include "../ConfigLib/Config.h"

void processTTLCMD(ST_File *p, char *filename)
{
    LogoDebug ("filename = %s\n FileList = %s\n", filename, p->szFileList);
    if (!g_cmd->bNeedCopyGui && NULL != strstr (p->szFileList, "koolsee.pro"))
    {
        //需要拷贝前台
        g_cmd->bNeedCopyGui = true;
    }
    else if (!g_cmd->bNeedCopyServer && NULL != strstr (p->szFileList, "GUIConfig.h"))
    {
        g_cmd->bNeedCopyServer = true;
        if (!g_cmd->bNeedCleanGui && NULL != strstr (filename, ".h"))
        {
            //需要重编前台
            g_cmd->bNeedCleanGui = true;
        }
    }
    else if (!g_cmd->bNeedCopyPlayerAPI && NULL != strstr (p->szFileList, "PlayerApi.cpp"))
    {
        //需要拷贝播放器库
        g_cmd->bNeedCopyPlayerAPI = true;
    }
    else if (!g_cmd->bNeedCopyUpdate && NULL != strstr (p->szFileList, "updateAPI.cpp"))
    {
        //拷贝升级进程
        g_cmd->bNeedCopyUpdate = true;
    }
}

void usage(int argc, char **argv)
{
    printf ("you input is:\n");
    for (int i = 0; i < argc; i++)
    {
        printf ("%s  ", argv[i]);
    }
    printf ("\nusage:\n"
            "%s -f filename: upload file\n"
            "%s -a: upload all the file you save\n"
            "%s -s filename: save the filename in project.txt\n"
            "%s -p project: specify the project name(must be specify)\n"
            , argv[0], argv[0], argv[0], argv[0]);
    fflush(stdout);
    getchar ();
    exit (-1);
}


void processParam(int argc, char **argv, struct ST_ArgvOpt *pParam)
{
    for (int i = 0; i < argc; ++i)
    {
        printf ("argv[%d] = [%s]\n", i, argv[i]);
        if (0 == strcmp(argv[i], "-s"))
        {
            //需要保存文件
            ++i;
            if (i >= argc)
            {
                usage(argc, argv);
            }
            strcpy (pParam->szSaveFile, argv[i]);
            printf ("pParam->szSaveFile = [%s]\n", pParam->szSaveFile);
            continue;
        }

        if (0 == strcmp(argv[i], "-a"))
        {
            //上传所有
            //LogoDebug ("find -a \n");
            pParam->bUploadAll = true;
            ++i;
            printf ("pParam->bUploadAll = true\n");
            continue;
        }

        if (0 == strcmp(argv[i], "-f"))
        {
            //需要上传文件
            ++i;
            if (i >= argc)
            {
                usage(argc, argv);
            }
            strcpy (pParam->szUploadFile, argv[i]);
            printf ("pParam->szUploadFile = [%s]\n", pParam->szUploadFile);
            continue;
        }

        if (0 == strcmp(argv[i], "-p"))
        {
            //记录工程的名字
            //LogoDebug("find -p \n");
            ++i;
            if (i >= argc)
            {
                usage(argc, argv);
            }
            strcpy (pParam->szProjectName, argv[i]);
            //LogoDebug("szProjectName = %s\n", pParam->szProjectName);
            printf ("szProjectName = %s\n", pParam->szProjectName);
            continue;
        }

    }

    if ('\0' == pParam->szProjectName[0])
    {
        //必须制定工程的名字
        printf ("must be specify the project name (-p project)\n");
        LogoDebug("must be specify the project name (-p project)\n");
        usage(argc, argv);
    }
    exitLine
}

bool saveFile(struct ST_ArgvOpt *pParam)
{
    FILE * fFileList;
    char buf[512];
    enterLine;
    getchar ();
    fFileList  = fopen(getSaveFile (pParam->szProjectName), "a+");
    enterLine;
    getchar ();
    if (NULL == fFileList)
    {
        LogoDebug ("open %s false!\n", getSaveFile (pParam->szProjectName));
        printf ("open %s false!\n", getSaveFile (pParam->szProjectName));
        return false;
    }

    while (0 == feof(fFileList))
    {
        //检查文件是否已经在保存列表中
        if (NULL == fgets(buf, sizeof (buf), fFileList))
        {
            LogoDebug("can't find %s in %s, need save.\n", pParam->szSaveFile, getSaveFile (pParam->szProjectName));
            break;
        }

//        LogoDebug ("buf = %s", buf);
        if (NULL != strstr (buf, pParam->szSaveFile))
        {
            //文件已经存在文件列表中，不需要保存
            LogoDebug ("find %s in %s. not need to save!!!", pParam->szSaveFile, getSaveFile (pParam->szProjectName));
            return true;
        }
    }

    fputs (pParam->szSaveFile, fFileList);
    fputs ("\n", fFileList);
    fclose (fFileList);
    //exitLine
    return true;
}

bool upload (sftp_session sftp, char *filename)
{
    ST_File *p = g_File;
    char strPath[Max_PathLen + 1] = {'\0'};
    char strName[Max_FileNameLen + 1] = {'\0'};
    //enterLine;
    if (!splitFilePath(filename, strPath, strName))
    {
        return false;
    }
    do
    {
        //搜索上传列表
        //LogoDebug ("LocalPath = %s\n", p->strPath[EN_LocalPath]);
        //LogoDebug ("ServerPath = %s\n", p->strPath[EN_ServerPath]);
        //LogoDebug("file list = %s\n", p->szFileList);
        if (NULL != strstr (p->strPath[EN_LocalPath], strPath) && NULL !=  strstr (p->szFileList, strName))
        {
            //已经找到上传路径
            //strServerPath = p->strPath[EN_ServerPath];
            //strLocalPath = p->strPath[EN_LocalPath];
            break;
        }
        p = p->next;
    }while (NULL != p);

    if (NULL == p)
    {
        //文件不在上传列表中
        LogoDebug ("can't find %s in config file \n", filename);
        printf ("can't find %s in config file \n", filename);
        fflush(stdout);
        getchar ();
        return false;
    }
    else
    {
        //检查文件是否是写保护，防止上传的文件不是最新的。
        int iResult = access(filename, R_OK | W_OK | F_OK);
        if (0 != iResult)
        {
#if 0
            if (ENOENT == iResult)
            {
                //文件不存在
                printf ("[%s] not exist", filename);
                fflush(stdout);
                getchar ();
                return false;
            }
            else if (EACCES == iResult)
            {
                //没有读写权限
                printf ("the file [%s], isn't check out, do you want to upload it??? [y/n]", filename);
                fflush(stdout);
                char input;
                scanf ("%c", &input);
                if ('y' != input && 'Y' != input)
                {
                    return false;
                }
            }
            else
            {
                printf ("the file [%s] Invalid parameter.", filename);
                fflush(stdout);
                getchar ();
                return false;
            }
#endif
        }

        //开始上传
        char rem_file[512];
        int iRet = -1;
        int iLoopTime = 0;
        strcpy (rem_file, p->strPath[EN_ServerPath]);
        strcat (rem_file, strName);

        //测试上传
        sftp_file to = sftp_open (sftp, rem_file, O_RDWR | O_CREAT | O_TRUNC, 0755);
        if (NULL == to)
        {
                printf ("sftp_open error [%s]\n", ssh_get_error(sftp->session));
            return 1;
        }

        FILE *from = fopen (filename, "r");
        if (NULL == from)
        {
            perror ("fopen");
            sftp_close(to);
            return -1;
        }
        printf("begin upload\n");
        int nbytes = -1;
        char buffer[4096];
        while((nbytes=fread (buffer, 1, sizeof (buffer), from)) > 0){
            if (nbytes != sftp_write (to, buffer, nbytes))
            {
                    printf ("sftp_write error [%s]\n", ssh_get_error(sftp->session));
            }
        }
        fclose (from);
        sftp_close(to);

        if (-1 != iRet)
        {
            //文件在上传列表中
            //根据上传的文件生成TTL命令
            processTTLCMD(p, strName);
            g_cmd->bNeedBuild = true;
            return true;
        }
        else
        {
            return false;
        }
    }
}


void uploadFileList(sftp_session sftp, struct ST_ArgvOpt *pParam)
{
    char filename[1024] = {'\0'};
    FILE * fFileList;
    char serverPath[512] = {'\0'};

    //fFileList  = fopen("saveFile.txt", "a+");
    fFileList  = fopen(getSaveFile (pParam->szProjectName), "r");
    if (NULL == fFileList)
    {
        LogoDebug("fFileList = NULL\n");
        printf ("open %s false!\n", getSaveFile (pParam->szProjectName));
        fflush(stdout);
        getchar ();
        exit (-1);
    }

    while (0 == feof(fFileList))
    {
        //读取文件
        if (NULL == fgets(filename, sizeof (filename), fFileList))
        {
            LogoDebug("fgets= NULL \n");
            break;
        }

        int iTempLen = strlen(filename) - 1;
        while (iTempLen >= 0)
        {
            if (('\n' == filename[iTempLen]) || ('\r' == filename[iTempLen]))
            {
                filename[iTempLen] = '\0';
                iTempLen--;
                continue;
            }

            break;
        }
        upload(sftp, filename);
        LogoDebug ("filename = %s\n", filename);
    }
    fclose (fFileList);

    //清空文件内容
    //fFileList  = fopen("saveFile.txt", "w");
    fFileList  = fopen(getSaveFile (pParam->szProjectName), "w");
    if (NULL == fFileList)
    {
        LogoDebug("fFileList = NULL\n");
        printf ("open %s false!\n", getSaveFile (pParam->szProjectName));
        fflush(stdout);
        getchar ();
        exit (-1);
    }
    fclose (fFileList);
}


int main(int argc, char **argv) {
    ssh_session session;
    ssh_channel channel;
    char buffer[4096];
    int nbytes;
    int rc;

//    for (int i = 0; i < argc; ++i)
//    {
//        printf ("argv[%d] = [%s]\n", i, argv[i]);
//    }

//    if (1 == argc)
//    {
//        usage (argc, argv);
//    }

    //处理命令行选项
    ST_ArgvOpt opt;
    processParam(argc, argv, &opt);
    setProjectPath (opt.szProjectName);

    //初始化日志
    if (initLog ("SIToollog.txt") < 0)
    {
        return 1;
    }

    if ('\0' != opt.szSaveFile[0])
    {
        saveFile(&opt);
    }

    if ('\0' != opt.szUploadFile[0] || opt.bUploadAll)
    {
        if (!readCfg(getConfigFile (opt.szProjectName), true))
        {
            printf ("false in read config file[%s]\n", getConfigFile (opt.szProjectName));
            LogoDebug ("false in read config file[%s]\n", getConfigFile (opt.szProjectName));
            fflush (stdout);
            getchar ();
            exit (-1);
        }
        session = connect_ssh (g_Server->strIP, g_Server->strUserName, 0);
        if (session == NULL) {
            ssh_finalize();
            return 1;
        }

        sftp_session sftp = sftp_new(session);
        sftp_init(sftp);

        if ('\0' != opt.szUploadFile[0])
        {
            upload(sftp, opt.szUploadFile);
        }

        if (opt.bUploadAll)
        {
            uploadFileList(sftp, &opt);
        }

        ssh_channel_send_eof(channel);
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        ssh_disconnect(session);
        ssh_free(session);
        ssh_finalize();
    }

#if 0
    //测试下载
    sftp_file from = sftp_open (sftp, "/home/yy/buildroot-2011.08/dl/qt-everywhere-opensource-src-4.7.3.tar.gz", O_RDONLY, 0755);
    if (NULL == from)
    {
        printf ("sftp_open error [%s]\n", ssh_get_error(session));
        goto sftp_failed;
    }

    FILE* to = fopen ("mytestfile", "w+");
    if (NULL == to)
    {
        perror ("fopen mytestfile");
        sftp_close(from);
        goto sftp_failed;
    }
    printf("begin download\n");
    while((nbytes=sftp_read(from, buffer, sizeof (buffer))) > 0){
        fwrite(buffer, 1, nbytes, to);
    }
    fclose (to);
    sftp_close(from);
    printf("download finished\n");

    //测试上传
    from = sftp_open (sftp, "/home/yy/mintpie P80 V1.3(1).rar", O_RDWR | O_CREAT | O_TRUNC, 0755);
    if (NULL == from)
    {
        printf ("sftp_open error [%s]\n", ssh_get_error(session));
        return 1;
    }

    to = fopen ("E:\\mintpie P80 V1.3(1).rar", "r");
    if (NULL == to)
    {
        perror ("fopen mytestfile");
        sftp_close(from);
        goto sftp_failed;
    }
    printf("begin upload\n");
    while((nbytes=fread (buffer, 1, sizeof (buffer), to)) > 0){
        if (nbytes != sftp_write (from, buffer, nbytes))
        {
            printf ("sftp_write error [%s]\n", ssh_get_error(session));
        }
    }
    fclose (to);
    sftp_close(from);
    printf("upload finished\n");

    //测试执行命令
    channel = ssh_channel_new(session);
    if (channel == NULL) {
        goto failed;
    }

    rc = ssh_channel_open_session(channel);
    if (rc < 0) {
        goto failed;
    }

    rc = ssh_channel_request_exec(channel, "/home/yy/Dvr_Pro/build.sh");
    if (rc < 0) {
        goto failed;
    }
#endif

    return 0;
failed:
    ssh_channel_close(channel);
    ssh_channel_free(channel);
sftp_failed:
    ssh_disconnect(session);
    ssh_free(session);
    ssh_finalize();
    return 1;
}
